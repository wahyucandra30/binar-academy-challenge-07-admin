const AdminSidebarSecondary = ({ path }) => {
    let header = "";
    let submenu = "";
    switch (path) {
        case "/admin":
            header = "DASHBOARD";
            submenu = "Dashboard";
            break;
        case "/admin/cars/add":
        case "/admin/cars":
            header = "CARS";
            submenu = "List Cars";
            break;
        default:
            break;
    }
    return <>
        <div className="min-w-[220px] bg-white h-full max-h-screen shadow-sm py-5 my-3">
            <h1 className="font-bold text-sm text-neutral-3 mx-4 mb-4">{header}</h1>
            <button className="w-full bg-darkblue-1 px-4 py-2 flex justify-start">
                <h2 className="font-bold text-sm text-black">{submenu}</h2>
            </button>
        </div>
    </>
}
export default AdminSidebarSecondary;