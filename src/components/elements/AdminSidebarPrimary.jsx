import { useNavigate } from "react-router-dom";

const AdminSidebarPrimary = ({ path }) => {
    const navigate = useNavigate()
    return <>
        <div className="min-w-[70px] h-full bg-darkblue-4 flex flex-col items-center">
            <div className="w-full h-[70px] flex justify-center items-center">
                <div className="w-[34px] h-[34px] bg-darkblue-0"></div>
            </div>
            <button onClick={() => navigate("/admin")} className={`w-full h-16 hover:bg-darkblue-2 flex flex-col items-center justify-center gap-1 hover:font-bold text-center
                 ${path === "/admin" ? "bg-darkblue-2" : ""}`}>
                <img src="/icon_home.svg" alt="" />
                <h1 className="text-white">Dashboard</h1>
            </button>
            <button onClick={() => navigate("/admin/cars")} className={`w-full h-16 hover:bg-darkblue-2 flex flex-col items-center justify-center gap-1 hover:font-bold text-center
                 ${path.includes("/admin/cars") ? "bg-darkblue-2" : ""}`}>                <img src="/icon_truck.svg" alt="" />
                <h1 className="text-white">Cars</h1>
            </button>
        </div>
    </>
}
export default AdminSidebarPrimary;