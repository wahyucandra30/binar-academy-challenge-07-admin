import formatIDR from "./utils/FormatIDR";
import { deleteDoc, doc } from 'firebase/firestore'
import { db } from "../firebase/firebase-config";
import { useNavigate } from "react-router-dom";
const CarCardAdmin = ({ data }) => {
    const navigate = useNavigate();
    const deleteCar = async (e) => {
        e.preventDefault();
        await deleteDoc(doc(db, `cars/${data.id}`))
        window.location.reload(false);
    }
    return (
        <div className="border flex flex-col justify-center items-center bg-white 
            w-full md:w-fit min-h-fit rounded-lg shadow-sm shadow-slate-300 p-6">
            <img src={data.image} alt="Car Preview" className="w-[270px] h-[160px]" />
            <div className="text-base text-left">
                <h2 className="mt-8">{data.name}</h2>
                <h1 className="font-bold mt-2">{formatIDR(data.price)} / hari</h1>
                <div className="flex flex-col gap-2 text-sm font-normal mt-2">
                    <div className="flex items-center gap-2">
                        <img src="/icon_key.png" alt="" />
                        Start rent - Finish rent
                    </div>
                    <div className="flex items-center gap-2">
                        <img src="/icon_clock2.png" alt="" className="w-[18px]" />
                        Updated at {data.dateUpdated.toDate().toLocaleDateString("en-GB", {
                            year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric'
                        })}
                    </div>
                </div>
                <div className="flex w-full mt-6 justify-center gap-4">
                    <button onClick={(e) => deleteCar(e)}
                    className="w-fit lg:w-36 h-12 hover:bg-red-500 border-red-500 border rounded-sm
                    hover:text-white text-red-500 stroke-red-500 hover:stroke-white">
                        <div className="flex justify-center text-sm font-bold gap-[10px]">
                            <svg className="fill-transparent" width="19" height="18" xmlns="http://www.w3.org/2000/svg">
                                <path d="M2.5 4.5H16M6.25 4.5V3a1.5 1.5 0 0 1 1.5-1.5h3a1.5 1.5 0 0 1 1.5 1.5v1.5m2.25 0V15a1.5 1.5 0 0 1-1.5 1.5H5.5A1.5 1.5 0 0 1 4 15V4.5h10.5Z"
                                    stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                            Delete
                        </div>
                    </button>
                    <button className="w-fit lg:w-36 h-12 bg-limegreen-4 hover:bg-limegreen-5 rounded-sm
                    hover:text-white text-white">
                        <div className="flex justify-center text-sm font-bold gap-[10px]">
                            <img src="/icon_edit.png" alt="" className="w-[18px] h-[18px]" />
                            Edit
                        </div>
                    </button>
                </div>
            </div>
        </div>
    )
}
export default CarCardAdmin;