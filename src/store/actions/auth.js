/* eslint-disable no-duplicate-case */
import {
    signInWithPopup,
    GoogleAuthProvider,
    FacebookAuthProvider,
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword
} from "firebase/auth"
import { auth } from "../../firebase/firebase-config"
import { browserLocalPersistence, setPersistence } from "firebase/auth"


//Note: Masih berantakan, parameternya masih tiki taka dengan UI. Mau diperbaiki tapi deadline. -wahyu
export const registerWithEmailandPassword = async (email, password, failed, success, alertMessage) => {
    if (email !== "" || password.length >= 6) {
        return setPersistence(auth, browserLocalPersistence)
            .then(async () => {
                try {
                    const data = await createUserWithEmailAndPassword(auth, email, password);
                    console.log(data);
                    failed(false);
                    success(true);
                }
                catch (err) {
                    console.log(err)
                    alertMessage(getErrorMessage(err.code));
                    failed(true);
                    success(false);
                }
            })
    }
    else {
        alertMessage("Masukkan username dan password yang benar. Perhatikan penggunaan huruf kapital.");
        failed(true);
        success(false);
        return null;
    }
}
export const loginWithEmailandPassword = async (dispatch, email, password, navigate, failed, alertMessage) => {
    if (email !== "" || password.length >= 6) {
        failed(false);
        return setPersistence(auth, browserLocalPersistence)
            .then(async () => {
                try {
                    const data = await signInWithEmailAndPassword(auth, email, password)
                    console.log("DATA: " + data)
                    dispatch({ type: "SET_USER_DATA", payload: data });

                    if (data.user?.email.includes("@admin.com")) {
                        navigate("/admin")
                    }
                    else {
                        navigate("/")
                    }
                }
                catch (err) {
                    alertMessage(getErrorMessage(err.code));
                    failed(true);
                    console.log(err);
                }
            })
    }
    else {
        alertMessage("Masukkan username dan password yang benar. Perhatikan penggunaan huruf kapital.");
        failed(true);
        return null;
    }
}
export const loginWithGoogle = (dispatch, navigate) => {
    const provider = new GoogleAuthProvider();
    return async () => {
        setPersistence(auth, browserLocalPersistence)
            .then(async () => {
                const data = await signInWithPopup(auth, provider)
                dispatch({ type: "SET_USER_DATA", payload: data });
                navigate("/");
            })

    }
}
export const loginWithFacebook = (dispatch, navigate) => {
    const provider = new FacebookAuthProvider();
    return async () => {
        setPersistence(auth, browserLocalPersistence)
            .then(async () => {
                const data = await signInWithPopup(auth, provider)
                dispatch({ type: "SET_USER_DATA", payload: data });
                navigate("/");
            })
    }
}
export const logOut = (dispatch, navigate) => {
    return auth.signOut()
        .then(() => {
            dispatch({ type: "SET_USER_DATA", payload: null });
            navigate("/login");
        });
}

const getErrorMessage = (errorCode) => {
    console.log("AUTH ERROR: " + errorCode)
    switch (errorCode) {
        case "ERROR_EMAIL_ALREADY_IN_USE":
        case "auth/account-exists-with-different-credential":
        case "auth/email-already-in-use":
            return "Email sudah terdaftar sebelumnya. Silakan pergi ke halaman login!";
        case "ERROR_WRONG_PASSWORD":
        case "auth/wrong-password":
            return "Masukkan username dan password yang benar. Perhatikan penggunaan huruf kapital.";
        case "ERROR_USER_NOT_FOUND":
        case "auth/user-not-found":
            return "Masukkan username dan password yang benar. Perhatikan penggunaan huruf kapital.";
        case "ERROR_USER_DISABLED":
        case "auth/user-disabled":
            return "User disabled.";
        case "ERROR_TOO_MANY_REQUESTS":
        case "auth/operation-not-allowed":
            return "Terlalu banyak permintaan login pada akun ini.";
        case "ERROR_OPERATION_NOT_ALLOWED":
        case "auth/operation-not-allowed":
            return "Server error, coba lagi nanti.";
        case "ERROR_INVALID_EMAIL":
        case "auth/invalid-email":
            return "Masukkan username dan password yang benar. Perhatikan penggunaan huruf kapital.";
        default:
            return "Login gagal. Tolong coba lagi.";
    }
}