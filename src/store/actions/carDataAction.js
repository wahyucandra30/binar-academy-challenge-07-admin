import axios from "axios";
import { carDataTypes } from "../actionTypes/carDataTypes";
import { db } from "../../firebase/firebase-config";
import { collection, getDocs, setDoc, doc, getDoc } from "firebase/firestore";
import { v4 } from "uuid";
export const getCarList = async (dispatch) => {
    dispatch({ type: carDataTypes.loading, payload: true });
    const data = await getDocs(collection(db, "cars"));
    await dispatch({ type: carDataTypes.list, payload: data.docs.map(doc => doc.data()) })
    dispatch({ type: carDataTypes.loading, payload: false })
}
export const addCarEntry = async (entry) => {
    await setDoc(doc(db, "cars", entry.id), entry)
}
// export const getCarList = async (dispatch) => {
//     dispatch({ type: carDataTypes.loading, payload: true });
//     return await axios
//         (process.env.REACT_APP_BASE_API_URL + "/admin/car")
//         .then(res => {
//             dispatch({
//                 type: carDataTypes.list,
//                 payload: res.data
//             })
//             .then(dispatch({ type: carDataTypes.loading, payload: false }))
//         })
// }
export const getCarById = async (dispatch, id) => {
    dispatch({ type: carDataTypes.loading, payload: true });
    const docRef = doc(db, `cars/${id}`);
    const docSnap = await getDoc(docRef);
    console.log("PILIH: " + JSON.stringify(docSnap.data(), 2));
    await dispatch({
        type: carDataTypes.selection,
        payload: docSnap.data()
    })
    dispatch({ type: carDataTypes.loading, payload: false });
    // return docSnap.data();
    // return await axios
    //     (process.env.REACT_APP_BASE_API_URL + "/admin/car/" + id)
    //     .then(res => {
    //         dispatch({
    //             type: carDataTypes.selection,
    //             payload: res.data
    //         })
    //             .then(dispatch({ type: carDataTypes.loading, payload: false }))
    //     })
}