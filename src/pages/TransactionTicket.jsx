import { Link, useNavigate } from "react-router-dom";
import PDFViewer from 'pdf-viewer-reactjs'
const TransactionTicket = () => {
    return <>
        <div className="flex flex-col items-center">
            <div className="flex flex-col gap-4 items-center my-10">
                <img src="/icon_success.svg" alt="" width={"48px"} height={"48px"} />
                <h1 className="font-bold text-sm">Pembayaran Berhasil</h1>
                <h2 className="text-sm text-neutral-3">Tunjukkan invoice ini ke petugas BCR di titik temu.</h2>
            </div>
            <div className="flex flex-col items-center w-[605px] h-fit bg-white 
            rounded-lg shadow-sm shadow-neutral-2 py-6 px-3 mb-8">
                <div className="flex w-full px-5 justify-between mb-6">
                    <div className="flex flex-col gap-4">
                        <h1 className="text-sm font-bold">Invoice</h1>
                        <h2 className="text-sm">*no invoice</h2>
                    </div>
                    <Link to={"/invoice.pdf"} target="_blank" download>
                        <button className="flex items-center justify-center 
                    bg-white border-darkblue-4 border w-[97px] h-9 rounded-sm">
                            <img src="/icon_download.svg" alt="" />
                            Unduh
                        </button>
                    </Link>
                </div>
                <div className="w-[557px] h-fit bg-neutral-2 rounded border">
                    <PDFViewer
                        document={{
                            url: '/invoice.pdf',
                        }}
                        scale={0.933}
                        hideNavbar={true}
                    />
                </div>
            </div>
        </div>
    </>
}

export default TransactionTicket;