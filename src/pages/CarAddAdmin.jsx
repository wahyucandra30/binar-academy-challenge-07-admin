import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCarList, addCarEntry } from "../store/actions/carDataAction";
import { storage } from "../firebase/firebase-config";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { v4 } from "uuid"
import { useNavigate } from "react-router-dom";
import swal from "sweetalert2"
const CarAddAdmin = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [name, setName] = useState("");
    const [price, setPrice] = useState(0);
    const [capacity, setCapacity] = useState(0);
    const [image, setImage] = useState(null);
    const addCar = async (e) => {
        e.preventDefault();
        if (image != null) {
            const imageRef = ref(storage, `carImages/${image.name + v4()}`)
            await uploadBytes(imageRef, image);
            const url = await getDownloadURL(imageRef);
            addCarEntry({
                name,
                capacity,
                image: url,
                price,
                productionYear: 2020,
                transmission: "Manual",
                dateUpdated: new Date(),
                id: v4()
            }).then(() => {
                swal.fire({
                    title: "Sukses!",
                    text: "Data berhasil ditambah",
                    type: "success"
                }).then(() => {
                    navigate("/admin/cars");
                });
            })
        }
    }
    useEffect(() => {
        document.title = "Car List";
        getCarList(dispatch)
    }, [dispatch]);
    return (<>
        <div className="flex flex-col w-full h-full items-start">
            <div className="flex w-fit text-xs gap-1 mb-6">
                <h2 className="font-bold m-0">Cars</h2>
                <img src="/icon_rightarrow.svg" alt="" />
                <h2 className="font-bold m-0">List Car</h2>
                <img src="/icon_rightarrow.svg" alt="" />
                <h2>Add New Car</h2>
            </div>
            <h1 className="text-xl font-bold bg-dark mb-4">Add New Car</h1>
            <div className="w-full">
                <div className="w-full px-5 py-3 flex flex-col justify-between mb-5 xl:mt-0 bg-white">
                    <div className="w-full py-2 flex flex-col justify-start gap-4">
                        <div className="flex items-center justify-between">
                            <label htmlFor="">Nama<span className="text-red-500">*</span></label>
                            <div className="w-3/4">
                                <input type="text"
                                    onChange={(e) => setName(e.target.value)}
                                    className="px-3 border border-neutral-2 rounded-sm w-1/2 h-9"
                                    placeholder="Placeholder" />
                            </div>
                        </div>
                        <div className="flex items-center justify-between">
                            <label htmlFor="">Harga<span className="text-red-500">*</span></label>
                            <div className="w-3/4">
                                <input type="number"
                                    onChange={(e) => setPrice(e.target.value)}
                                    className="px-3 border border-neutral-2 rounded-sm w-1/2 h-9"
                                    placeholder="Placeholder" />
                            </div>
                        </div>
                        <div className="flex items-center justify-between">
                            <label htmlFor="">Kapasitas<span className="text-red-500">*</span></label>
                            <div className="w-3/4">
                                <input type="number"
                                    onChange={(e) => setCapacity(e.target.value)}
                                    className="px-3 border border-neutral-2 rounded-sm w-1/2 h-9"
                                    placeholder="Placeholder" />
                            </div>
                        </div>
                        <div className="flex items-center justify-between">
                            <label htmlFor="">Foto<span className="text-red-500">*</span></label>
                            <div className="w-3/4">
                                <div className=" px-2 border border-neutral-2 rounded-sm w-1/2 h-9 flex items-center">
                                    <input type="file"
                                        onChange={(e) => {
                                            setImage(e.target.files[0])
                                        }}
                                        placeholder="Placeholder" />
                                </div>
                            </div>
                        </div>
                        <div className="flex items-center justify-between">
                            <label htmlFor="">Start Rent</label>
                            <span className="flex items-center w-3/4 h-9">-</span>
                        </div>
                        <div className="flex items-center justify-between">
                            <label htmlFor="">Finish Rent</label>
                            <span className="flex items-center w-3/4 h-9">-</span>
                        </div>
                        <div className="flex items-center justify-between">
                            <label htmlFor="">Created At</label>
                            <span className="flex items-center w-3/4 h-9">-</span>
                        </div>
                        <div className="flex items-center justify-between">
                            <label htmlFor="">Updated At</label>
                            <span className="flex items-center w-3/4 h-9">-</span>
                        </div>
                    </div>
                </div>
                <div className="flex gap-4">
                    <button onClick={() => navigate("/admin/cars")}
                        className="border border-darkblue-4 bg-white hover:bg-darkblue-1 px-3 py-2 text-sm font-bold rounded-sm
                text-darkblue-4">
                        Cancel
                    </button>
                    <button onClick={(e) => addCar(e)} className="bg-darkblue-4 hover:bg-darkblue-5 px-3 py-2 text-sm font-bold rounded-sm
                text-white">
                        Save
                    </button>
                </div>
            </div>
        </div >
    </>
    )

}

export default CarAddAdmin;