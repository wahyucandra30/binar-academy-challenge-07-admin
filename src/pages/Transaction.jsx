import { Outlet, useNavigate } from "react-router-dom";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer"

const Transaction = () => {
    const navigate = useNavigate();
    return <>
        <Navbar />
        <div className="flex items-end bg-darkblue-0 w-full h-[164px] pt-24">
            <div className="flex flex-col md:flex-row justify-between w-full px-28 2xl:px-64">
                <div className="flex items-start h-fit w-fit gap-[21px]">
                    <button onClick={() => navigate(-1)}>
                        <img src="/icon_leftarrow.svg" alt="" width={"24px"} height={"24px"} />
                    </button>
                    <div className="flex flex-col pt-0.5 mb-4">
                        <h1 className="text-sm font-bold">Tiket</h1>
                        <h2 className="text-xs">Order ID: xxxxxxxx</h2>
                    </div>
                </div>
                <div className="flex items-center gap-3 mb-[45px]">
                    <div className="flex gap-2 items-center">
                        <div className="flex justify-center items-center rounded-full bg-darkblue-4 w-[14px] h-[14px]">
                            <img src="/icon_checkmark.svg" alt="" />
                        </div>
                        <h2>Pilih Metode</h2>
                    </div>
                    <div class="relative flex py-3 items-center">
                        <div class="flex-grow border-t w-7 border-darkblue-4"></div>
                    </div>
                    <div className="flex gap-2 items-center">
                        <div className="flex justify-center items-center rounded-full bg-darkblue-4 w-[14px] h-[14px]">
                            <img src="/icon_checkmark.svg" alt="" />
                        </div>
                        <h2>Bayar</h2>
                    </div>
                    <div class="relative flex py-3 items-center">
                        <div class="flex-grow border-t w-7 border-darkblue-4"></div>
                    </div>
                    <div className="flex gap-2 items-center">
                        <div className="flex justify-center items-center rounded-full bg-darkblue-4 w-[14px] h-[14px]">
                            <h6 className="text-white text-[10px]">3</h6>
                        </div>
                        <h2>Tiket</h2>
                    </div>
                </div>
            </div>
        </div>
        <Outlet />
        <Footer />
    </>
}

export default Transaction;