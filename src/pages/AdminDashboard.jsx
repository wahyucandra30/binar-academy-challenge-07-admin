import { Outlet, useNavigate, useLocation } from "react-router-dom";
import { logOut } from "../store/actions/auth"
import { useDispatch, useSelector } from "react-redux";
import AdminSidebarSecondary from "../components/elements/AdminSidebarSecondary";
import AdminSidebarPrimary from "../components/elements/AdminSidebarPrimary";
import { useEffect, useState } from "react";
import { auth } from "../firebase/firebase-config";
const AdminDashboard = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const location = useLocation();
    const [currentAdmin, setCurrentAdmin] = useState("");
    useEffect(() => {
        auth.onAuthStateChanged(user => {
            if (user) {
                setCurrentAdmin(user.email)
                if(!user.email.includes("@admin.com") && location.pathname.includes("/admin"))
                {
                    navigate("/");
                }
            }
            else if (location.pathname !== "/register") {
                navigate("/login")
            }
        })
    })
    return <>
        <div className="w-screen h-screen flex flex-row justify-center overflow-hidden  ">
            <AdminSidebarPrimary path={location.pathname} />
            <div className="w-full h-full flex flex-col">
                <div className="w-full bg-white min-h-[70px] shadow z-10 flex items-center px-4 justify-between">
                    <div className="bg-darkblue-1 w-[100px] h-[34px]"></div>
                    <div className="w-auto flex justify-between gap-7 items-center">
                        <div className="flex items-center">
                            <img src="/icon_search.svg" alt="" className="absolute ml-3" />
                            <input type="text" className="border rounded-sm pl-9 w-[174px] h-[34px]" placeholder="Search" />
                            <button className="border border-darkblue-4 rounded-sm px-4 h-[34px] bg-white hover:bg-darkblue-4
                        text-darkblue-4 hover:text-white font-bold">Search</button>
                        </div>
                        <div className="flex items-center gap-2">
                            <div className="rounded-full bg-darkblue-1 max-w-[38px] max-h-[38px] min-w-[38px] min-h-[38px] flex items-center justify-center
                        text-darkblue-4 font-bold text-sm text-center">
                                {currentAdmin?.charAt(0)?.toUpperCase()}
                            </div>
                            <h1 className="font-bold">{currentAdmin}</h1>
                            <img src="/icon_downarrow.svg" alt="" />
                        </div>
                        <div>
                        </div>
                    </div>
                </div>
                <div className="flex w-full h-full">
                    <AdminSidebarSecondary path={location.pathname} />
                    <div className="w-full h-full max-h-screen bg-neutral-100 overflow-y-auto px-5 py-6">
                        {location.pathname === "/admin" &&
                            <>
                                <h1 className="text-3xl font-bold mb-2">Welcome, Admin BCR</h1>
                                <button onClick={() => logOut(dispatch, navigate)}
                                    className="bg-red-600 hover:bg-red-700 px-5 py-2 rounded-sm text-white font-bold">Sign out</button>
                            </>
                        }
                        <Outlet />
                    </div>
                </div>
            </div>
        </div>
    </>
}
export default AdminDashboard;