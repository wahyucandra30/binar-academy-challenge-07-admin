import { useEffect } from "react";
import CarCardAdmin from "../components/CarCardAdmin";
import { useDispatch, useSelector } from "react-redux";
import { getCarList } from "../store/actions/carDataAction";
import Loading from "../components/elements/Loading";
import ImageButton from "../components/elements/ImageButton";

const CarListAdmin = () => {
    const { carList, isLoading } = useSelector((state) => state.carDataReducer);
    const dispatch = useDispatch();
    const handleCarList = () => {
        return (
            <>
                {!isLoading ? carList?.map((car) => {
                    return (
                        <div>
                            <CarCardAdmin data={car} />
                        </div>)
                })
                    : <Loading />
                }
            </>
        )
    }
    useEffect(() => {
        document.title = "Car List";
        getCarList(dispatch)
    }, [dispatch]);
    return (<>
        <div className="flex flex-col w-full max-w-screen-2xl min-h-full items-start mb-16">
            <div className="flex w-fit text-xs gap-1 mb-6">
                <h2 className="font-bold m-0">Cars</h2>
                <img src="/icon_rightarrow.svg" alt="" />
                <h2>List Car</h2>
            </div>
            <div className="flex w-full justify-between items-center">
                <h1 className="text-xl font-bold bg-dark">List Car</h1>
                <ImageButton linkTo={"./add"}
                    bgColor={"bg-darkblue-4"} bgHoverColor={"hover:bg-darkblue-5"}
                    placeholder={"Add New Car"} iconURL="/icon_plus.svg" iconWidth={"18px"} iconHeight={"18px"}
                    defaultHeight={"h-9"} defaultWidth={"w-[142px]"} iconBgColor={"bg-transparent"} />
            </div>
            <div className="flex gap-4 mb-6">
                    <button className="border border-darkblue-4 bg-darkblue-1 px-3 py-2 text-sm font-bold rounded-sm
                text-darkblue-4">
                    All
                </button>
                <button className="border border-darkblue-2 bg-white px-3 py-2 text-sm font-bold rounded-sm
                text-darkblue-1">
                    Small
                </button>
                <button className="border border-darkblue-2 bg-white px-3 py-2 text-sm font-bold rounded-sm
                text-darkblue-1">
                    Medium
                </button>
                <button className="border border-darkblue-2 bg-white px-3 py-2 text-sm font-bold rounded-sm
                text-darkblue-1">
                    Large
                </button>
            </div>
            <div className="w-full px-0 justify-start flex mb-16 xl:mt-0">
                <div className="grid grid-cols-1 lg:grid-cols-2 xl:grid-cols-3 gap-6">
                    {handleCarList()}
                </div>
            </div>
        </div>
    </>
    )

}

export default CarListAdmin;